import styles from "../styles/card.module.css"
import Link from 'next/link'

export default function Card({ text }) {
    return (
        <>
            <div className={styles.square}></div>

            <Link href="/" >
                <a className={styles.card}>
                    <p>{text}</p>
                </a>
            </Link>
        </>
    )
}