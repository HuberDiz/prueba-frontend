import styles from '../styles/prod.module.css'
import Image from 'next/image'
import { useState } from 'react/cjs/react.development';

let listFav = []

export default function Product({ props, setFavList }) {
  if (typeof window !== "undefined") {
    let sessionList = []
    try{
      sessionList = JSON.parse(localStorage.getItem("favs"))
    } catch (error){
      localStorage.setItem("favs",JSON.stringify(listFav))
    }
    listFav = sessionList
  }
  const [favorites, setFavorites] = useState(listFav)
  function checkDupe(list, element2Check) {
    let isDuped = false;
    list.forEach(element => {
      if (element.productSku == element2Check.productSku) {
        isDuped = true;
      }
    });
    return isDuped
  }

  function isFunction(variable) {
    let state = true;
    try {
      variable()
    } catch {
      state = false;
    }
    return state;
  }

  function addToFav(prod) {
    let product = {
      productName: prod.productName,
      productImageUrl: prod.productImageUrl,
      productPrice: prod.productPrice,
      productSku: prod.productSku
    }
    let listProd = []
    if (localStorage.getItem("favs") != null || localStorage.getItem("favs") != undefined) {
      listProd = JSON.parse(localStorage.getItem("favs"))
      let isDuped = checkDupe(listProd, product)
      if (!isDuped) {
        listProd.push(product)
        setFavorites(listProd)
      } else {
        let pos = listProd.map(function (e) { return e.productSku }).indexOf(product.productSku)
        listProd.splice(pos, 1)
        if (isFunction(setFavList)) {
          setFavList(listProd)
        } else {
          setFavorites(listProd)
        }
      }
      localStorage.setItem("favs", JSON.stringify(listProd))
    } else {
      listProd.push(product)
      localStorage.setItem("favs", JSON.stringify(listProd))
    }
  }

  return (
    <div className={styles.product}>
      {props.map((prod) => {
        var color = "transparent"
        var stroke = "#000000"
        if (listFav != null) {
          listFav.map(fav => {
            if (fav.productSku == prod.productSku) {
              color = "#E02020"
              stroke = "transparent"
            }
          })
        }
        return (
          <div key={prod.productSku} className={styles.prodContainer}>
            <div className={styles.prodImg}>
              <button onClick={() => { addToFav(prod); }}>
                <svg
                  width="23px"
                  height="21px"
                  viewBox="0 0 23 21"
                  version="1.1"
                >
                  <title>Path Copy</title>
                  <g
                    id="Page-1"
                    stroke="none"
                    strokeWidth="1"
                    fill="none"
                    fillRule="evenodd"
                  >
                    <g
                      id="Propuesta-Home---D"
                      transform="translate(-1354.000000, -21.000000)"
                      fill={color}
                      stroke={stroke}
                      strokeWidth="1.3"
                    >
                      <g id="Group" transform="translate(0.000000, 16.000000)">
                        <path
                          d="M1365.58956,24.9791953 C1364.32495,23.9707257 1358.97472,19.5892635 1355.83501,15.1792328 C1354.14483,12.4595075 1355.21896,8.80273402 1357.28639,7.08766916 C1359.54709,5.21642961 1362.45265,5.77827762 1365.26249,8.62941819 C1365.28293,8.64941617 1365.30802,8.65893902 1365.33125,8.67417557 C1365.35913,8.69417355 1365.38514,8.71893296 1365.41581,8.73131266 C1365.43997,8.74178779 1365.46505,8.74083551 1365.49014,8.74654922 C1365.52359,8.75416749 1365.55611,8.7665472 1365.58956,8.7665472 C1365.62487,8.7665472 1365.65832,8.75416749 1365.6927,8.74654922 C1365.71686,8.74083551 1365.74102,8.74083551 1365.76332,8.73131266 C1365.79677,8.71798067 1365.82372,8.69226899 1365.85345,8.67131872 C1365.87389,8.65608216 1365.89805,8.64846389 1365.91664,8.62941819 C1368.72834,5.77732533 1371.63482,5.21738189 1373.89181,7.08766916 C1375.96017,8.80273402 1377.0343,12.4604598 1375.34412,15.1792328 C1372.20627,19.5873589 1366.85511,23.9697734 1365.58956,24.9791953"
                          id="Path-Copy"
                        ></path>
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
              <svg className={styles.placeholder} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 48">
                <title>Asset 69</title>
                <g id="Layer_2" data-name="Layer 2">
                  <g id="Layer_1-2" data-name="Layer 1">
                    <path fill="#dadada" d="M56,0H8A8,8,0,0,0,0,8V40a7.27,7.27,0,0,0,.48,2.64A8.05,8.05,0,0,0,4,46.91,8.1,8.1,0,0,0,8,48H56a8.08,8.08,0,0,0,6.72-3.65A8.18,8.18,0,0,0,64,40V8A8,8,0,0,0,56,0ZM33,42.67H8a2.35,2.35,0,0,1-.83-.16L21.65,22.75,33.52,34.61l3.76,3.76,4.27,4.3ZM58.67,40A2.68,2.68,0,0,1,56,42.67H49.12l-8-8.06,6.82-6.74,10.78,12Zm0-8.13L50,22.21a2.72,2.72,0,0,0-1.89-.88,2.53,2.53,0,0,0-1.95.78l-8.82,8.74L23.23,16.77A2.84,2.84,0,0,0,21.12,16a2.76,2.76,0,0,0-1.95,1.09L5.33,36V8A2.68,2.68,0,0,1,8,5.33H56A2.68,2.68,0,0,1,58.67,8Z" />
                  </g>
                </g>
              </svg>
              <Image
                src={prod.productImageUrl}
                width={250}
                height={150}
              />
            </div>
            <div className={styles.prodDetails}>
              <p>
                <b>{prod.productName}</b>
              </p>
              <p>{prod.productPrice}€</p>
            </div>
          </div>
        )
      })}
    </div>
  )
}
