import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react/cjs/react.development'
import {useRouter} from "next/router"
import styles from '../styles/header.module.css'
import Search from './Search'

export default function Header() {
    var router = useRouter();
    var color = "transparent"
    if (router.pathname == "/favorites"){
        color = "#000000"
    }
    
    return (
        <header className={styles.navbar}>
            <div className={styles.logo}>
                <Link href="/" >
                    <a>
                        <Image
                            src="/../public/images/Logo_KaveHome.png"
                            width={120}
                            height={21}
                            layout="responsive"
                        />
                    </a>
                </Link>
            </div>
            <Search/>
            <Link href="favorites" >
                <button className={styles.fav}>
                    <svg width="23px" height="21px" viewBox="0 0 23 21" version="1.1">
                        <title>Path Copy</title>
                        <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                            <g id="Propuesta-Home---D" transform="translate(-1354.000000, -21.000000)" fill={color} stroke="#000000" strokeWidth="1.3">
                                <g id="Group" transform="translate(0.000000, 16.000000)">
                                    <path d="M1365.58956,24.9791953 C1364.32495,23.9707257 1358.97472,19.5892635 1355.83501,15.1792328 C1354.14483,12.4595075 1355.21896,8.80273402 1357.28639,7.08766916 C1359.54709,5.21642961 1362.45265,5.77827762 1365.26249,8.62941819 C1365.28293,8.64941617 1365.30802,8.65893902 1365.33125,8.67417557 C1365.35913,8.69417355 1365.38514,8.71893296 1365.41581,8.73131266 C1365.43997,8.74178779 1365.46505,8.74083551 1365.49014,8.74654922 C1365.52359,8.75416749 1365.55611,8.7665472 1365.58956,8.7665472 C1365.62487,8.7665472 1365.65832,8.75416749 1365.6927,8.74654922 C1365.71686,8.74083551 1365.74102,8.74083551 1365.76332,8.73131266 C1365.79677,8.71798067 1365.82372,8.69226899 1365.85345,8.67131872 C1365.87389,8.65608216 1365.89805,8.64846389 1365.91664,8.62941819 C1368.72834,5.77732533 1371.63482,5.21738189 1373.89181,7.08766916 C1375.96017,8.80273402 1377.0343,12.4604598 1375.34412,15.1792328 C1372.20627,19.5873589 1366.85511,23.9697734 1365.58956,24.9791953" id="Path-Copy"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                </button>
            </Link>
        </header>
    )
}