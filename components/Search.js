import { useState } from "react/cjs/react.development";
import { getFromLocal } from "../logic/getFrom";
import styles from "../styles/header.module.css"
import SearchResults from "../components/SearchResults"

export default function Search({customList}) {
    const [list, setList] = useState(getFromLocal("allProds"))
    const [results, setResults] = useState([]);
    const [isFocused, setFocus] = useState(false)
    let resultsList = []
    function SearchFilter(value) {
        resultsList = []
        list.forEach(element => {
            if (element.productName.toLowerCase().includes(value.toLowerCase())) {
                resultsList.push(element)
            }
        });
    }

    function onEnter(event) {
        if (event.target.value.length == 0) {
            setResults([])
        }
        else if (event.target.value.length >= 3) {
            SearchFilter(event.target.value)
            setResults(resultsList)
        }
    }

    return (
        <div className={styles.search} id="searchdiv">
            <input id="in" type="text" onChange={(event) => { onEnter(event) }} onFocus={()=>{setFocus(true)}} onBlur={() => {setFocus(false)}} placeholder="Buscar productos" />
            <SearchResults results={results} isFocused={isFocused}/>
        </div>
    )
}