import { getFromLocal } from "../logic/getFrom";

export default function SearchFilter({value, setList}){
    let list = getFromLocal("allProds")
    let results = []
    list.forEach(element => {
        if(element.productName.includes(value)){
            results.push(element)
        }
    });
    setList(results)
}