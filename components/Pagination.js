import { useState } from "react"
import styles from "../styles/pagination.module.css"

const PRODS_PER_PAGE = 12

export function maxPages(array) {
    let maxPages = Math.ceil(array.length / PRODS_PER_PAGE);
    return maxPages;
}

function getPages(number) {
    let pages = [];
    for (let i = 0; i < number; i++) {
        pages.push(i);
    }
    return (pages);
}

export default function Pagination({ props, actualPage, previusPage, nextPage, changePage }) {
    const MAX_PAGES = maxPages(props)
    var allPages = getPages(MAX_PAGES)
    let pages
    if (actualPage < 7) {
        pages = allPages.slice(0, 8)
    } else {
        pages = allPages.slice(actualPage-4, actualPage+4)

    }

    return (
        <div className={styles.paginationMenu}>
            {
                actualPage > 0 &&
                
                <button className={styles.arrowButton} onClick={previusPage}>
                    &#60;
                </button>

            }
            {
                pages.map(numPage => {
                    return (
                        <button className={numPage == actualPage ? styles.actual : ""} key={"page" + numPage} onClick={() => { changePage(numPage) }}>
                            {numPage + 1}
                        </button>
                    )
                })
            }
            {
                actualPage < MAX_PAGES-1 &&

                <button className={styles.arrowButton} onClick={nextPage}>
                    &#62;
                </button>
            }

        </div>
    )
}