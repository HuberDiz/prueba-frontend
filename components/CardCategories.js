import styles from "../styles/home.module.css"
import Card from "../components/card"

export default function CardCategories() {
    const cardCategories = ["We are Kave", "Estancias", "Muebles", "Decoracion", "Proyectos", "Estilos"]
    return (
            <ul className={styles.cardCategories}>
                {cardCategories.map((category, index) => {
                    return (
                        <li key={"cardCategory" + index}>
                            <Card text={category} />
                        </li>)
                })}
            </ul>
    )
}