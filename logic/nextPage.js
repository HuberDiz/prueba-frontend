import changePage from "../logic/changePage"

function nextPage(numPage, prodList , setFavList, setNumPage, PRODS_PER_PAGE) {
    let next = numPage + 1
    changePage(next, prodList, setFavList, setNumPage, PRODS_PER_PAGE)
    setNumPage(next)
}
export default nextPage