export function getFromLocal(name){
    let object = []
    if(typeof window !== "undefined"){
        object = JSON.parse(localStorage.getItem(name))
    }
    return object
}

export function getFromSession(name){
    let object = []
    if(typeof window !== "undefined"){
        object = JSON.parse(sessionStorage.getItem(name))
    }
    return object
}