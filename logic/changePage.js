function changePage(num, array, setState, setNumPage, PRODS_PER_PAGE) {
    setNumPage(num)
    setState(array.slice(num * PRODS_PER_PAGE, (num * PRODS_PER_PAGE) + PRODS_PER_PAGE))
}

export default changePage