import changePage from "../logic/changePage"

function previusPage (numPage, prodList, setFavList, setNumPage, PRODS_PER_PAGE) {
    let previous = numPage - 1
    changePage(previous, prodList, setFavList, setNumPage, PRODS_PER_PAGE)
}

export default previusPage