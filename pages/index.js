import Link from 'next/link'
import styles from "../styles/home.module.css"
import Card from "../components/card"
import Product from "../components/Product"
import { useState } from 'react'
import CardCategories from '../components/CardCategories'

export async function getStaticProps() {

  const res = await fetch("https://kavehome.com/nfeeds/es/templatebuilder/20210201")
  const allprods = await res.json()
  let randomList = []
  let random;
  let prods = []
  for (let i = 0; i < 9; i++) {
    random = Math.floor(Math.random() * 3079)
    while (randomList[i] == random) {
      random = Math.floor(Math.random() * 3079)
    }
    randomList.push(random)
    prods.push(allprods.results[random])
  }

  return {
    props: {
      prods,
    }
  }
}

export default function Home({ prods }) {
  const [products, setProducts] = useState(prods)
  const singleCategories = ["Estancias", "Estilos", "Muebles", "Decoración", "We are Kave", "Proyectos"]


  


  return (
    <>
      <section className={styles.homePage}>
        <div className={styles.homeImage}>
          <div className={styles.homeTitle}>
            <h1>
              Cuando la realidad supera la ficción.
          </h1>
            <h1>
              Trucos para estar en casa.
          </h1>
          </div>
        </div>
        <div className={styles.categories}>
          <h1>Inspírate</h1>
          <ul className={styles.simpleCategories}>
            {
              singleCategories.map((category, index) => {
                return (
                  <li key={"singleCategory" + index}>
                    <Link href="/">
                      <a>{category}</a>
                    </Link>
                  </li>
                )
              })
            }
          </ul>
          <CardCategories/>
        </div>
        <Product props={products} />
        <Link href="/Catalog">
          <button className={styles.allProds}>VER TODOS LOS PRODUCTOS</button>
        </Link>
      </section>
    </>
  )
}

