import styles from "../styles/catalog.module.css"
import ProductList from "./ProductList"
import { useState } from "react/cjs/react.development"
import Product from "../components/Product";
import Pagination from "../components/Pagination"
import nextPage from "../logic/nextPage"
import changePage from "../logic/changePage"
import previusPage from "../logic/previusPage"

const PRODS_PER_PAGE = 12

export async function getStaticProps() {

    const res = await fetch("https://kavehome.com/nfeeds/es/templatebuilder/20210201")
    const data = await res.json()
    let allProds = data.results

    return {
        props: {
            allProds,
        }
    }
}

export default function Catalog({ allProds }) {


    const [page, setPage] = useState(allProds.slice(0, PRODS_PER_PAGE))
    const [numPage, setNumPage] = useState(0)

    return (
        <section className={styles.prodPage}>
            <h1>Products</h1>
            <h3>Lorem ipsum dolor sit amet</h3>
            <Product props={page} />
            <Pagination
                props={allProds}
                actualPage={numPage}
                previusPage={() => previusPage(numPage, allProds, setPage, setNumPage, PRODS_PER_PAGE)}
                nextPage={() => nextPage(numPage, allProds, setPage, setNumPage, PRODS_PER_PAGE)}
                changePage={num => { changePage(num, allProds, setPage, setNumPage, PRODS_PER_PAGE) }}
            />
        </section>
    )
}