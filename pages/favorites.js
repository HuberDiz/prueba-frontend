import React, { useEffect } from "react";
import { useState } from "react/cjs/react.development";
import styles from "../styles/catalog.module.css"
import Product from "../components/Product"
import Pagination from "../components/Pagination"
import nextPage from "../logic/nextPage"
import changePage from "../logic/changePage"
import previusPage from "../logic/previusPage"

var prodList = [];
let initialList;

const PRODS_PER_PAGE = 12

export default function Favorites() {
    if (typeof window !== "undefined") {
        let sessionList = []
        try {
            sessionList = JSON.parse(window.localStorage.getItem("favs"));
        } catch (error) {
            localStorage.setItem("favs", JSON.stringify(prodList))
        }
        prodList = sessionList
        if (prodList == null){
            initialList = prodList
        } else {
            initialList = prodList.slice(0, PRODS_PER_PAGE)
        }
    }
    const [favList, setFavList] = useState(initialList)
    const [numPage, setNumPage] = useState(0)

    return (
        <section className={styles.prodPage}>
            <h1>Lista de favoritos</h1>
            <h3>Lorem ipsum dolor sit amet</h3>
            {
                favList != null &&
                <>
                    <Product props={favList} setFavList={setFavList} />
                    <Pagination
                        props={prodList}
                        actualPage={numPage}
                        previusPage={() => previusPage(numPage, prodList, setFavList, setNumPage, PRODS_PER_PAGE)}
                        nextPage={() => nextPage(numPage, prodList, setFavList, setNumPage, PRODS_PER_PAGE)}
                        changePage={num => { changePage(num, prodList, setFavList, setNumPage, PRODS_PER_PAGE) }}
                    />
                </>
            }
        </section>
    )
}