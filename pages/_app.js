import '../styles/globals.css'
import Header from '../components/header'
import getAllProducts from '../logic/getProducts'

function MyApp({ Component, pageProps }) {
  getAllProducts();
  return (
    <>
      <Header/>
      <Component {...pageProps} />
    </>
  )

}

export default MyApp
