import { useState } from "react";
import Product from "../components/Product";
import Pagination from "../components/Pagination"
import styles from "../styles/product_list.module.css"

const PRODS_PER_PAGE = 12

export default function ProductList({ allProds }) {
    
    const [page, setPage] = useState(allProds.slice(0, PRODS_PER_PAGE))
    const [numPage, setNumPage] = useState(0)
    function changePage(num, array) {
        setNumPage(num)
        setPage(array.slice(num * PRODS_PER_PAGE, (num * PRODS_PER_PAGE) + PRODS_PER_PAGE))
    }
    const previusPage = () => {
        let previous = numPage - 1
        changePage(previous, allProds)
    }
    const nextPage = () => {
        let next = numPage + 1
        changePage(next, allProds)
        setNumPage(next)

    }
    return (
        <>
            <Product props={page}/>
            <Pagination
                props={allProds}
                actualPage={numPage}
                previusPage={previusPage}
                nextPage={nextPage}
                changePage={num => { changePage(num, allProds) }}
            />
        </>
    )
}

